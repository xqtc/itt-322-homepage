#!/usr/bin/python
# author: FelixEcker / Bertrahm

import os
import sys
import time
from pathlib import Path
from bs4 import BeautifulSoup
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

URL = 'https://mese.webuntis.com/WebUntis/?school=BK-Technik-Siegen#/basic/timetable'
DATE = ''
OUTPATH = ''

def main():
    print ('> Starting firefox in headless mode...')
    options = Options()
    options.headless = True
    options.set_preference('browser.download.folderList', 2)
    if OUTPATH == '':
        options.set_preference('browser.download.dir', os.getcwd())
    else:
        options.set_preference('browser.download.dir', OUTPATH)
    browser = webdriver.Firefox(options = options)
    browser.get(URL)

    try:
        print ('> Loading website...')
        # Wait until the input is there
        elem = WebDriverWait(browser, 30).until(
            EC.presence_of_element_located((By.TAG_NAME, "input")) #This is a dummy element
        )

        # Enter our class
        element_enter = browser.find_element(By.TAG_NAME, "input")
        element_enter.send_keys("ITT322")
        element_enter.send_keys(Keys.ENTER)

        if not DATE == '':
            element_enter = browser.find_element(By.CLASS_NAME, 'un-datetime-picker__input')
            element_enter.send_keys(DATE)
            element_enter.send_keys(Keys.ENTER)

        elem = WebDriverWait(browser, 30).until(
                EC.presence_of_element_located((By.CLASS_NAME, "timetableContent"))
        )
        print ('> Loaded! Downloading ICS...')

        browser.find_element(By.XPATH, "//button[@title='ICS Kalender']").click()
        time.sleep(5)

        print ('Saved at '+OUTPATH)
    finally:
        browser.quit()
        print ('> Finished!')


if __name__ == '__main__':
    for i in range(1, len(sys.argv)):
        if sys.argv[i] == '-d':
            DATE = sys.argv[i+1]
            i += 1
            continue

        if sys.argv[i] == '-o':
            OUTPATH = str(Path(sys.argv[i+1].replace(' ', '\\ ')).resolve())
            print (OUTPATH)
            i += 1
            continue
        
        if sys.argv[i] == '-h' or sys.argv[i] == '--help':
            print ('Usage: python untisscraper.py [options]')
            print ()
            print ('Options:')
            print ('-h --help       Display this help')
            print ('-d DD.MM.YYYY   Scrape for given date')
            print ('-o <path>       Set path to download to')
            exit()

    main()