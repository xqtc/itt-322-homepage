<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DiscordController;
use App\Http\Controllers\ResourceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('index');

Route::get('/login', [DiscordController::class, 'login'])->name('login');
Route::get('/logout', [DiscordController::class, 'logout'])->name('logout');
Route::get('/auth', [DiscordController::class, 'auth']);

Route::get('resources', [ResourceController::class, 'resources'])->name('resources');
