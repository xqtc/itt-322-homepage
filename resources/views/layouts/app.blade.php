<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>ITT-322</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="bg-dark w-100 h-100 text-white">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow shadow-lg">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">ITT322</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('index') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://mese.webuntis.com/WebUntis/?school=BK-Technik-Siegen#/basic/timetable" target="_blank">Stundenplan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('resources') }}">Ressourcen</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Sitemap
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="#">Cloud</a></li>
                            <li><a class="dropdown-item" href="https://bin.itt322.de">Pastebin</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="https://c.tenor.com/h6T7YUwIn5UAAAAC/blahaj-go-spinny-blahaj.gif">Blåhaj go SPINNY!</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled">Disabled</a>
                    </li>
                </ul>
                <a href="https://gitlab.com/CommandCrafterHD/itt-322-homepage" target="_blank" class="btn btn-primary me-3"><i class="fa-brands fa-git-alt me-2"></i>Source Code</a>
                    @if($user == false)
                    <a href="{{ route('login') }}" class="btn btn-primary"><i class="fa-brands fa-discord me-2"></i>Login</a>
                    @else
                    <div class="dropstart">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa-solid fa-user me-2"></i> {{ $user->username }}
                        </button>
                        <ul class="dropdown-menu dropdown-menu-dark">
                            <li><a class="dropdown-item disabled" disabled href="#" title="Kommt noch...">Admin interface</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item bg-warn" href="{{ route('logout') }}"><i class="fa-solid fa-door-open me-2"></i> Logout</a></li>
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </nav>
        <div class="w-100 h-100">
            @yield('body')
        </div>
    </body>
    </html>
